//! Kraken challange: Read in CSV data and calculates the account 

use std::{fs, env, error::Error, io, process, collections::HashMap};
use serde::Deserialize;
use csv::{Reader, Writer};

/// This holds the data from the CSV file
type ClientTransactions = HashMap<u32, Transaction>;

/// This holds the output data per client
type ClientAccount = HashMap<u32, Account>;



/// This struct defines the main application
///
/// # Attributes
/// * data_in: HashMap with key = tx, value = Object of account numbers
/// * data_out: String to write as CSV
#[derive(Debug, Deserialize)]
struct Application{
    data_in: ClientTransactions,
    data_out: ClientAccount
}

impl Application{

    /// Contructor
    fn new(data: String) -> Result<Self, Box<dyn Error>>{

        // Locals
        let mut data_in = ClientTransactions::new();
        let mut data_out =  ClientAccount::new();

        // Read in CSV data
        let mut rdr = csv::Reader::from_reader(data.as_bytes());

        // create data_in HashMap
        for result in rdr.deserialize() {
            let record: Transaction = result?;
            // Insert Raw data
            data_in.insert(record.tx , record);
        }

        // Create data_out defaults
        for (_key, value) in &data_in{
            // Is client alreay in list?
            match data_out.get(&(value.client as u32)){
                Some(_i) => continue,
                // If not, then add default struct
                None => {
                    let client = value.client;
                    let client_account = Account{
                        available: 0.0,
                        held: 0.0,
                        total: 0.0,
                        locked: false,
                    };
                    // Insert default account
                    data_out.insert(client as u32, client_account);
                }
            }
        }

        // Return instance
        Ok(Self {
            data_in: data_in,
            data_out: data_out
        })
    }

    // Create output
    fn calculate(&mut self) -> Result<(), Box<dyn Error>> {

        // Iterate through data_in and calculate account numbers
        for (_key, value) in &self.data_in{

            // match the account action
            match value.type_.as_str() {

                // available: increase
                // total: increase
                "deposit" => {
                    // get the account details
                    let account = self.data_out.get_mut(&(value.client as u32)).unwrap();

                    // perform required operations
                    account.available += value.amount;
                    account.total += value.amount;
                }, 
                
                // available: decrease
                // total: decrease
                "withdrawal" => {
                    // get the account details
                    let account = self.data_out.get_mut(&(value.client as u32)).unwrap();

                    // perform required operations
                    account.available -= value.amount;
                    account.total -= value.amount;
                }, 

                // available: decrease
                // held: increase
                "dispute" => {
                    // get the account details
                    let account = self.data_out.get_mut(&(value.client as u32)).unwrap();

                    // perform required operations
                    account.available -= value.amount;
                    account.held += value.amount;
                }, 
                // available: increase
                // held: decrease
                "resolve" => {
                    // get the account details
                    let account = self.data_out.get_mut(&(value.client as u32)).unwrap();

                    // perform required operations
                    account.available += value.amount;
                    account.held -= value.amount;
                }, 

                // held: decrease
                // total: decrease
                // frozen true => abort
                "chargeback" => {
                    // get the account details
                    let account = self.data_out.get_mut(&(value.client as u32)).unwrap();

                    // perform required operations
                    account.available += value.amount;
                    account.total += value.amount;
                    account.locked = true;
                }, 

                // else panic
                _ => panic!("no valid account operation! call the staff please")
            }

        }

        // Iterate through data_out and and set freeze
        for (_key, value) in &mut self.data_out{
            // If we are in minus, freeze
            if value.total <= 0.0 {

                // set locked to true
                value.locked = true;
            }
        }

        Ok(())
    }

    /// generate output string
    fn gen_output(&mut self) -> Result<(),  Box<dyn Error>>{

        // CSV writer
        let mut wtr = csv::Writer::from_writer(io::stdout());

        wtr.write_record(&["client", "available", "held", "total", "locked"])?;

        for (key, value) in &mut self.data_out {
            wtr.write_record(&[&key.to_string(), &value.available.to_string(), &value.held.to_string(), &value.total.to_string(), &value.locked.to_string()])?;
        }

        Ok(())
    }

}


/// This struct defines a user
///
/// # Attributes
/// * type_ (String)
/// * client (u16)
/// * tx (u32)
/// * amount (f32)
#[derive(Debug, Deserialize)]
struct Transaction {
    // since type is predefined rename it here
    #[serde(rename = "type")]
    type_: String,
    client: u16,
    tx: u32,
    amount: f32,
}

/// This struct defines a user
///
/// # Attributes
/// * type_ (String)
/// * client (u16)
/// * tx (u32)
/// * amount (f32)
#[derive(Debug, Deserialize)]
struct Account {
    available: f32,
    held: f32,
    total: f32,
    locked: bool,
}



fn main() {

    // ------------------ Init       ---------------------- //
    // get the function arguments
    let args: Vec<String> = env::args().collect();

    // Read data as String and del whitespaces
    let mut foo: String = fs::read_to_string(&args[1]).unwrap();
    foo = foo.replace(" ", "");

    // ------------------ Create App ---------------------- //
    let mut app = match Application::new(foo){
        Ok(data) => data,
        Err(e) => panic!("{:?}", e),
    };

    // ------------------ Handle Data ---------------------- //
    match app.calculate(){
        Ok(_i) =>() ,
        Err(e) => panic!("{:?}", e),
    }

    // ------------------ Write report ---------------------- //

    match app.gen_output(){
        Ok(_data) => (),
        Err(e) => panic!("{:?}", e),
    }


    // Exit process correct
    process::exit(0)
}
